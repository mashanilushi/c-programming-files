#include <stdio.h>

int main()
{
    int r1,c1,r2,c2,i,j,k;
    int sum = 0;
    printf("Enter the number of rows in the first matrix: ");
    scanf("%i",&r1);
    printf("Enter the number of columns in the first matrix: ");
    scanf("%i",&c1);
    printf("Enter the number of rows in the second matrix: ");
    scanf("%i",&r2);
    printf("Enter the number of columns in the second matrix: ");
    scanf("%i",&c2);
    int m1[r1][c1];
    printf("Enter the numbers in the first matrix: \n");
    for (i = 0 ; i < r1 ; i++){
        for (j = 0 ; j < c1 ; j++){
            scanf("%i",&m1[i][j]);
        }
    }
    int m2[r2][c2];
    printf("Enter the numbers in the second matrix: \n");
    for (i = 0 ; i < r2 ; i++){
        for (j = 0 ; j < c2 ; j++){
            scanf("%i",&m2[i][j]);
        }
    }
    int add[r1][c1];
    printf("The addition of the matrices are: \n");
    if (r1 == r2 && c1 == c2){
        for (i = 0 ; i < r1 ; i++){
            for (j = 0 ; j < c1 ; j++){
                add[i][j] = m1[i][j] + m2[i][j];
                printf("\t%i",add[i][j]);
            }
            printf("\n");
        }
    }
    else {
        printf("Can not add the matrices.\n");
    }

    int mul[r1][c2];
    printf("\nThe multiplication of the matrices are: \n");
    if (c1 == r2){
        for (i = 0 ; i < r1 ; i++){
            for (j = 0 ; j < c2 ; j++){
                for (k = 0 ; k < c1 ; k++){
                    sum = sum + m1[i][k] * m2[k][j];
                }
                mul[i][j] = sum;
                sum = 0;
                printf("\t%i",mul[i][j]);
            }
            printf("\n");
        }
    }
    else {
        printf("Can not multiply the matrices.\n");
    }
    return 0;
}