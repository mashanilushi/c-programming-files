#include <stdio.h>
#include <string.h>

int main()
{
    int x = 0;
    char s[100];
    printf("Enter a sentence: ");
    gets(s);
    printf("Enter the character: ");
    char c = getchar();
    int length = strlen(s);
    printf("Frequency of the letter: ");
    for (int i = 0 ; i < length ; i++){
        if (s[i] == c){
            x++;
        }
    }
    printf("%i",x);
    return 0;
}
