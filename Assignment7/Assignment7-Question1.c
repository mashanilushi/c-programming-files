#include <stdio.h>
#include <string.h>

int main()
{
    int length;
    char s[100];
    printf("Enter a sentence: ");
    gets(s);
    length = strlen(s);
    printf("The reversed sentence is: ");
    for (int i = length - 1 ; i >=0 ; i--){
        printf("%c",s[i]);
    }
    return 0;
}