#include <stdio.h>

struct Student{
    char name[20];
    char subject[20];
    int marks;
};

int main()
{
    int num;
    printf("Enter the number of students : ");
    scanf("%i",&num);
    if (num >= 5){
        struct Student s[num];
        for (int i = 0 ; i < num ; i++){
            printf("Enter the first name: ");
            scanf("%s",&s[i].name);
            printf("Enter the subject: ");
            scanf("%s",&s[i].subject);
            printf("Enter the marks: ");
            scanf("%i",&s[i].marks);
        }
        int x = 1;
        for (int i = 0 ; i < num ; i++){
            printf("\nStudent no. %i\n",x);
            printf("Name: %s\n",s[i].name);
            printf("Subject: %s\n",s[i].subject);
            printf("Marks: %i\n",s[i].marks);
            x++;
        }
    }
    else {
        printf("Please enter information of at least 5 students.");
    }
    return 0;

}