#include <stdio.h>

int main()
{
	float num;
	printf("Enter a number: ");
	scanf("%f",&num);
	if (num>0)
		printf("Number is a positive number");
	else if (num<0)
		printf("Number is a negative number");
	else
		printf("You entered 0");
	return 0;
}