#include <stdio.h>
int main()
{
	int n,i=1,x=1,answer;
	printf("Enter a number: ");
	scanf("%i",&n);
	while (i<=n){
        if (x == 1){
            printf("Multiplication Table of %i\n",i);
        }
        if (x<=12){
         answer = x*i;
         printf("%i * %i = %i\n",i,x,answer);
         x++;
        }
		else {
            x=1;
            i++;
		}
	}
	return 0;
}