#include <stdio.h>
int main()
{
	int num,i,new_num = 0;
	printf("Enter a number: ");
	scanf("%i",&num);
	while (num != 0){
		i = num % 10;
		new_num = new_num * 10 + i;
		num /= 10;
	}
	printf("New number = %i",new_num);
	return 0;
}