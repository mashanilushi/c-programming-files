#include <stdio.h>

int printFibonacciSequence(int x);
int main()
{
    int num,i;
    printf("Enter a number: ");
    scanf("%i",&num);
    for (int i = 0 ; i <= num ; i++){
        printf("%i\n",printFibonacciSequence(i));
    }
    return 0;
}

int printFibonacciSequence(int x){
    if (x > 1){
        return printFibonacciSequence(x-2) + printFibonacciSequence(x-1);
    }
    else if (x == 0){
        return 0;
    }
    else if (x == 1){
        return 1;
    }
}