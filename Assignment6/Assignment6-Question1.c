#include <stdio.h>

void printNumberTriangle(int x);
int main()
{
    int num;
    printf("Enter a number: ");
    scanf("%i",&num);
    printNumberTriangle(num);
    return 0;
}

void printNumberTriangle(int x){
    if (x >= 1){
        printNumberTriangle(x-1);
        if (x != 1){
            printf("\n");
        }
        for (  ; x != 0 ; x--){
            printf("%i",x);
        }
    }

}