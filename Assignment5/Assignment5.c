#include <stdio.h>

int getAttendance(int price);
int getProfit(int price, int people);

int main()
{
    int people;
    int price, profit;
    for (price = 5 ; price < 50 ; price = price + 5){
        people = getAttendance(price);
        profit = getProfit(price,people);
        if (profit>0){
            printf("Price of the Ticket = Rs. %i     Profit = Rs. %i\n\n",price,profit);
        }
        else {
            printf("Price of the Ticket = Rs. %i     Loss = Rs. %i\n\n",price,profit);
        }
    }
    return 0;
}
int getAttendance(int price){
    int x;
    int people;
    x = (price - 15)/5;
    people = 120 - 20 * x;
    if (people < 0){
        people = 0;
    }
    return people;

}
int getProfit(int price, int people){
    int profit;
    profit = (people * price) - (500 + 3 * people);
    return profit;
}
